﻿using Lib.Shared;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Lib.Core
{
    /// <summary>
    /// Объект для текстового вывода
    /// </summary>
    public static class OutputWriter
    {
        static Lazy<TextWriter> writer = new Lazy<TextWriter>(() => {
            var assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                var fileOutput = $"{Path.GetDirectoryName(assembly.Location)}\\log\\{Environment.UserName}_{DateTime.Now.Writable()}.txt";
                var info = new FileInfo(fileOutput);
                if (!info.Directory.Exists)
                {
                    try
                    {
                        info.Directory.Create();
                    }
                    catch (Exception ex)
                    {
                        Output.TraceError(ex);
                        fileOutput = null;
                    }
                }

                if (fileOutput != null)
                {
                    try
                    {
                        return TextWriter.Synchronized(new StreamWriter(fileOutput));
                    }
                    catch (Exception ex)
                    {
                        Output.TraceError(ex);
                    }
                }
            }
            return null;
        }, true);
        /// <summary>
        /// Объект для записи
        /// </summary>
        static TextWriter Writer {
            get
            {
                return writer.Value;
            }
        }

        /// <summary>
        /// Зарегистрировать вывод
        /// </summary>
        public static void Register()
        {
            Output.Traced += async (data) => await OnTraceAsync(data);
        }

        private static async Task OnTraceAsync(OutputData obj)
        {            
            await Writer?.WriteAsync(obj.Text);
            if (obj.Additional != null) await Writer?.WriteAsync(obj.Additional);
            await Writer?.FlushAsync();
        }
    }
}
