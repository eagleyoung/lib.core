﻿using Lib.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Lib.Core
{
    /// <summary>
    /// Класс для работы с конфигурационными файлами
    /// </summary>
    public class Config : BaseConfig
    {
        static Lazy<Config> instance = new Lazy<Config>(() => {
            List<KeyValuePair<string, string>> parsed = new List<KeyValuePair<string, string>>();

            var assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                var cfgFolderPath = $"{Path.GetDirectoryName(assembly.Location)}\\cfg";
                var cfgFolder = new DirectoryInfo(cfgFolderPath);
                if (!cfgFolder.Exists)
                {
                    try
                    {
                        cfgFolder.Create();
                    }
                    catch (Exception ex)
                    {
                        Output.TraceError(ex);
                        return new Config();
                    }
                }

                var cfgFiles = cfgFolder.GetFiles("*.ini", SearchOption.TopDirectoryOnly);
                foreach (var file in cfgFiles)
                {
                    var name = file.Name.Substring(0, file.Name.Length - file.Extension.Length);
                    foreach (var line in File.ReadAllLines(file.FullName))
                    {
                        var trim = line.Trim();
                        if (trim.StartsWith("//") || trim.Length == 0) continue;

                        var equalIndex = trim.IndexOf('=');
                        if (equalIndex >= 0)
                        {
                            var dataKey = trim.Substring(0, equalIndex).Trim();
                            var key = $"{name}.{dataKey}";
                            var value = trim.Substring(equalIndex + 1).Trim();
                            parsed.Add(new KeyValuePair<string, string>(key, value));
                        }
                    }
                }
            }

            return new Config(parsed);
        }, true);
        public static Config Instance => instance.Value;

        protected Config() { }
        protected Config(List<KeyValuePair<string, string>> parsed) : base(parsed) { }
    }
}
