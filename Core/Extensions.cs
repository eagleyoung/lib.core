﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Lib.Core
{
    /// <summary>
    /// Расширения классов
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Получить все доступные файлы по указанному пути
        /// </summary>
        public static List<string> GetFiles(this string path)
        {
            var ret = new List<string>();
            GetFilesRecursive(path, ret);
            return ret;
        }

        static void GetFilesRecursive(string path, List<string> found)
        {
            try
            {
                foreach (var file in Directory.GetFiles(path, "*", SearchOption.TopDirectoryOnly))
                {
                    found.Add(file);
                }
                foreach (var dir in Directory.GetDirectories(path, "*", SearchOption.TopDirectoryOnly))
                {
                    GetFilesRecursive(dir, found);
                }
            }
            catch (UnauthorizedAccessException)
            {
                return;
            }
        }
    }
}
